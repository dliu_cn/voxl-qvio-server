
#define CHAR_WIDTH  5
#define CHAR_HEIGHT 7


/**
 * 01110
 * 10001
 * 10001
 * 11111
 * 10001
 * 10001
 * 10001
 */
#define cCharacter_A 0x3a31fc631

/**
 * 11110
 * 10001
 * 10001
 * 11110
 * 10001
 * 10001
 * 11110
 */
#define cCharacter_B 0x7a31f463e

/**
 * 01110
 * 10001
 * 10000
 * 10000
 * 10000
 * 10001
 * 01110
 */
#define cCharacter_C 0x3a308422e

/**
 * 11110
 * 10001
 * 10001
 * 10001
 * 10001
 * 10001
 * 11110
 */
#define cCharacter_D 0x7a318c63e

/**
 * 11111
 * 10000
 * 10000
 * 11110
 * 10000
 * 10000
 * 11111
 */
#define cCharacter_E 0x7e10f421f

/**
 * 11111
 * 10000
 * 10000
 * 11110
 * 10000
 * 10000
 * 10000
 */
#define cCharacter_F 0x7e10f4210

/**
 * 01110
 * 10001
 * 10000
 * 10000
 * 10111
 * 10001
 * 01110
 */
#define cCharacter_G 0x3a3085e2e

/**
 * 10001
 * 10001
 * 10001
 * 11111
 * 10001
 * 10001
 * 10001
 */
#define cCharacter_H 0x4631fc631

/**
 * 11111
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 11111
 */
#define cCharacter_I 0x7c842109f

/**
 * 01111
 * 00001
 * 00001
 * 00001
 * 01001
 * 01001
 * 00110
 */
#define cCharacter_J 0x3c210a526

/**
 * 10001
 * 10010
 * 10100
 * 11000
 * 10010
 * 10010
 * 10001
 */
#define cCharacter_K 0x4654c4a51

/**
 * 10000
 * 10000
 * 10000
 * 10000
 * 10000
 * 10000
 * 11111
 */
#define cCharacter_L 0x42108421f

/**
 * 10001
 * 11011
 * 10101
 * 10101
 * 10001
 * 10001
 * 10001
 */
#define cCharacter_M 0x4775ac631

/**
 * 10001
 * 10001
 * 11001
 * 10101
 * 10011
 * 10001
 * 10001
 */
#define cCharacter_N 0x4639ace31

/**
 * 01110
 * 10001
 * 10001
 * 10001
 * 10001
 * 10001
 * 01110
 */
#define cCharacter_O 0x3a318c62e

/**
 * 11110
 * 10001
 * 10001
 * 11110
 * 10000
 * 10000
 * 10000
 */
#define cCharacter_P 0x7a31f4210

/**
 * 01110
 * 10001
 * 10001
 * 10101
 * 10101
 * 10010
 * 01101
 */
#define cCharacter_Q 0x3a31ad64d

/**
 * 11110
 * 10001
 * 10001
 * 11110
 * 10100
 * 10010
 * 10001
 */
#define cCharacter_R 0x7a31f5251

/**
 * 01110
 * 10001
 * 10000
 * 01110
 * 00001
 * 10001
 * 01110
 */
#define cCharacter_S 0x3a307062e

/**
 * 11111
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 */
#define cCharacter_T 0x7c8421084

/**
 * 10001
 * 10001
 * 10001
 * 10001
 * 10001
 * 10001
 * 01110
 */
#define cCharacter_U 0x46318c62e

/**
 * 10001
 * 10001
 * 10001
 * 10001
 * 01010
 * 01010
 * 00100
 */
#define cCharacter_V 0x46318a944

/**
 * 10001
 * 10001
 * 10001
 * 10001
 * 10101
 * 10101
 * 01010
 */
#define cCharacter_W 0x46318d6aa

/**
 * 10001
 * 10001
 * 01010
 * 00100
 * 01010
 * 10001
 * 10001
 */
#define cCharacter_X 0x462a22a31

/**
 * 10001
 * 10001
 * 01010
 * 00100
 * 00100
 * 00100
 * 00100
 */
#define cCharacter_Y 0x462a21084

/**
 * 11111
 * 00001
 * 00010
 * 00100
 * 01000
 * 10000
 * 11111
 */
#define cCharacter_Z 0x7c222221f

/**
 * 01110
 * 10001
 * 10011
 * 10101
 * 11001
 * 10001
 * 01110
 */
#define cCharacter_0 0x3a33ae62e

/**
 * 00100
 * 01100
 * 10100
 * 00100
 * 00100
 * 00100
 * 11111
 */
#define cCharacter_1 0x11942109f

/**
 * 01110
 * 10001
 * 00001
 * 00010
 * 00100
 * 01000
 * 11111
 */
#define cCharacter_2 0x3a211111f

/**
 * 01110
 * 10001
 * 00001
 * 00110
 * 00001
 * 10001
 * 01110
 */
#define cCharacter_3 0x3a213062e

/**
 * 00010
 * 00110
 * 01010
 * 10010
 * 11111
 * 00010
 * 00010
 */
#define cCharacter_4 0x8ca97c42

/**
 * 11111
 * 10000
 * 11110
 * 00001
 * 00001
 * 10001
 * 01110
 */
#define cCharacter_5 0x7e1e0862e

/**
 * 01110
 * 10001
 * 10000
 * 11110
 * 10001
 * 10001
 * 01110
 */
#define cCharacter_6 0x3a30f462e

/**
 * 11111
 * 00001
 * 00010
 * 00100
 * 01000
 * 01000
 * 01000
 */
#define cCharacter_7 0x7c2222108

/**
 * 01110
 * 10001
 * 10001
 * 01110
 * 10001
 * 10001
 * 01110
 */
#define cCharacter_8 0x3a317462e

/**
 * 01110
 * 10001
 * 10001
 * 01111
 * 00001
 * 10001
 * 01110
 */
#define cCharacter_9 0x3a317862e

/**
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 * 11111
 */
#define cCharacter__ 0x1f

/**
 * 01110
 * 10101
 * 10100
 * 01110
 * 00101
 * 10101
 * 01111
 */
#define cCharacter_dollar 0x3ab4716af

/**
 * 11000
 * 11001
 * 00010
 * 00100
 * 01000
 * 10011
 * 00011
 */
#define cCharacter_percent 0x632222263

/**
 * 00000
 * 00000
 * 00000
 * 00000
 * 01100
 * 00100
 * 01000
 */
#define cCharacter_comma 0x3088

/**
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 * 01100
 * 01100
 */
#define cCharacter_period 0x18c

/**
 * 01000
 * 01000
 * 01000
 * 01000
 * 01000
 * 00000
 * 01000
 */
#define cCharacter_exclamation 0x210842008

/**
 * 01100
 * 10010
 * 00010
 * 00100
 * 01000
 * 00000
 * 01000
 */
#define cCharacter_question 0x324222008

/**
 * 00010
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 00010
 */
#define cCharacter_leftparen 0x88421082

/**
 * 01000
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 01000
 */
#define cCharacter_rightparen 0x208421088

/**
 * 00000
 * 00000
 * 00000
 * 01110
 * 00000
 * 00000
 * 00000
 */
#define cCharacter_dash 0x70000

/**
 * 00000
 * 00000
 * 00100
 * 01110
 * 00100
 * 00000
 * 00000
 */
#define cCharacter_plus 0x471000

/**
 * 00001
 * 00001
 * 00010
 * 00100
 * 01000
 * 10000
 * 10000
 */
#define cCharacter_slash 0x42222210

/**
 * 00000
 * 01010
 * 00000
 * 00100
 * 00000
 * 10001
 * 01110
 */
#define cCharacter_at 0x1402022e

/**
 * 00000
 * 01010
 * 00000
 * 00100
 * 00000
 * 01110
 * 10001
 */
#define cCharacter_tilde 0x140201d1

/**
 * 00000
 * 00100
 * 01010
 * 10001
 * 00000
 * 00000
 * 00000
 */
#define cCharacter_power 0x8a88000

/**
 * 00000
 * 01010
 * 01010
 * 00000
 * 00000
 * 00000
 * 00000
 */
#define cCharacter_dquote 0x14a00000

/**
 * 01010
 * 01010
 * 11111
 * 01010
 * 11111
 * 01010
 * 01010
 */
#define cCharacter_pound 0x295f57d4a

/**
 * 01110
 * 10000
 * 10000
 * 01000
 * 10101
 * 10010
 * 01101
 */
#define cCharacter_ampersand 0x3a104564d

/**
 * 00000
 * 00100
 * 00100
 * 00000
 * 00000
 * 00000
 * 00000
 */
#define cCharacter_quote 0x8400000

/**
 * 00000
 * 00100
 * 11111
 * 00100
 * 01010
 * 10001
 * 00000
 */
#define cCharacter_asterix 0x9f22a20

/**
 * 00000
 * 01100
 * 01100
 * 00000
 * 01100
 * 01100
 * 00000
 */
#define cCharacter_colon 0x18c03180

/**
 * 00000
 * 01100
 * 01100
 * 00000
 * 01100
 * 00100
 * 01000
 */
#define cCharacter_semicolon 0x18c03088

/**
 * 11100
 * 10000
 * 10000
 * 10000
 * 10000
 * 10000
 * 11100
 */
#define cCharacter_leftbracket 0x72108421c

/**
 * 00111
 * 00001
 * 00001
 * 00001
 * 00001
 * 00001
 * 00111
 */
#define cCharacter_rightbracket 0x1c2108427

/**
 * 10000
 * 10000
 * 01000
 * 00100
 * 00010
 * 00001
 * 00001
 */
#define cCharacter_backslash 0x420820821

/**
 * 00000
 * 00010
 * 00100
 * 01000
 * 00100
 * 00010
 * 00000
 */
#define cCharacter_lt 0x4441040

/**
 * 00000
 * 01000
 * 00100
 * 00010
 * 00100
 * 01000
 * 00000
 */
#define cCharacter_gt 0x10411100

/**
 * 00000
 * 00000
 * 01110
 * 00000
 * 01110
 * 00000
 * 00000
 */
#define cCharacter_equal 0xe03800

/**
 * 00010
 * 00100
 * 00100
 * 01000
 * 00100
 * 00100
 * 00010
 */
#define cCharacter_leftbrace 0x88441082

/**
 * 01000
 * 00100
 * 00100
 * 00010
 * 00100
 * 00100
 * 01000
 */
#define cCharacter_rightbrace 0x208411088

/**
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 * 00100
 */
#define cCharacter_bar 0x108421084

/**
 * 00000
 * 00000
 * 01000
 * 10101
 * 00010
 * 00000
 * 00000
 */
#define cCharacter_squiggle 0x8a8800

/**
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 * 00000
 */
#define cCharacter_empty 0x0

/**
 * 10101
 * 01010
 * 10101
 * 01010
 * 10101
 * 01010
 * 10101
 */
#define cCharacter_invalid 0x555555555

//Order the characters to match ascii order starting at 32 (space)
uint64_t cCharacters[95] = { 
    cCharacter_empty,
    cCharacter_exclamation,
    cCharacter_dquote,
    cCharacter_pound,
    cCharacter_dollar,
    cCharacter_percent,
    cCharacter_ampersand,
    cCharacter_quote,
    cCharacter_leftparen,
    cCharacter_rightparen,
    cCharacter_asterix,
    cCharacter_plus,
    cCharacter_comma,
    cCharacter_dash,
    cCharacter_period,
    cCharacter_slash,
    cCharacter_0,
    cCharacter_1,
    cCharacter_2,
    cCharacter_3,
    cCharacter_4,
    cCharacter_5,
    cCharacter_6,
    cCharacter_7,
    cCharacter_8,
    cCharacter_9,
    cCharacter_colon,
    cCharacter_semicolon,
    cCharacter_lt,
    cCharacter_equal,
    cCharacter_gt,
    cCharacter_question,
    cCharacter_at,
    cCharacter_A,
    cCharacter_B,
    cCharacter_C,
    cCharacter_D,
    cCharacter_E,
    cCharacter_F,
    cCharacter_G,
    cCharacter_H,
    cCharacter_I,
    cCharacter_J,
    cCharacter_K,
    cCharacter_L,
    cCharacter_M,
    cCharacter_N,
    cCharacter_O,
    cCharacter_P,
    cCharacter_Q,
    cCharacter_R,
    cCharacter_S,
    cCharacter_T,
    cCharacter_U,
    cCharacter_V,
    cCharacter_W,
    cCharacter_X,
    cCharacter_Y,
    cCharacter_Z,
    cCharacter_leftbracket,
    cCharacter_backslash,
    cCharacter_rightbracket,
    cCharacter_power,
    cCharacter__,
    cCharacter_tilde,
    cCharacter_A,
    cCharacter_B,
    cCharacter_C,
    cCharacter_D,
    cCharacter_E,
    cCharacter_F,
    cCharacter_G,
    cCharacter_H,
    cCharacter_I,
    cCharacter_J,
    cCharacter_K,
    cCharacter_L,
    cCharacter_M,
    cCharacter_N,
    cCharacter_O,
    cCharacter_P,
    cCharacter_Q,
    cCharacter_R,
    cCharacter_S,
    cCharacter_T,
    cCharacter_U,
    cCharacter_V,
    cCharacter_W,
    cCharacter_X,
    cCharacter_Y,
    cCharacter_Z,
    cCharacter_leftbrace,
    cCharacter_bar,
    cCharacter_rightbrace,
    cCharacter_squiggle
};

#define getRow(c,y) ((c >> ((CHAR_HEIGHT - y - 1) * CHAR_WIDTH)) & 0x1f)
#define getIndex(row,x) ((row >> (CHAR_WIDTH - x - 1)) & 1)
#define getValue(c,x,y) (getIndex(getRow(c,y),x)) 

#define plus_inner_vals {\
    {0,0},\
    {0,1},\
    {0,2},\
    {1,0},\
    {2,0},\
    {0,-1},\
    {0,-2},\
    {-1,0},\
    {-2,0}}

#define plus_outer_vals {\
    { 1, 1},\
    { 1, 2},\
    { 1, 3},\
    { 0, 3},\
    {-1, 3},\
    {-1, 2},\
    {-1, 1},\
    {-2, 1},\
    {-3, 1},\
    {-3, 0},\
    {-3,-1},\
    {-2,-1},\
    {-1,-1},\
    {-1,-2},\
    {-1,-3},\
    { 0,-3},\
    { 1,-3},\
    { 1,-2},\
    { 1,-1},\
    { 2,-1},\
    { 3,-1},\
    { 3, 0},\
    { 3, 1},\
    { 2, 1}}

#define validLoc(w,h,x,y) (((x) >= 0) && ((y) >= 0) && ((x) < w) && ((y) < h))


static uint64_t cCharacter_get_format(char c){

    if(c >= ' ' && c <= '~')
        return cCharacters[c - ' '];

    return cCharacter_invalid;
}

static void cCharacter_draw_char(uint8_t *frame, int width, int height, char c, int x, int y, int value){

    uint64_t ch = cCharacter_get_format(c);
    for(int i = 0; i < CHAR_HEIGHT; i++){
        uint64_t row = getRow(ch, i);
        for(int j = 0; j < CHAR_WIDTH; j++){
            if(getIndex(row, j)){
                if(validLoc(width, height, (y+i), (x+j)))
                    frame[(width * (y + i)) + (x + j)] = value;
            }
        }
    }
}
static void cCharacter_draw_dchar(uint8_t *frame, int width, int height, char c, int x, int y, int value){

    uint64_t ch = cCharacter_get_format(c);
    for(int i = 0; i < CHAR_HEIGHT; i++){
        uint64_t row = getRow(ch, i);
        for(int j = 0; j < CHAR_WIDTH; j++){
            if(getIndex(row, j)){
                if(validLoc(width, height, (x+(j*2)), (y+(i*2))))
                    frame[(width * (y + (i * 2)))     + (x + (j * 2))]     = value;
                if(validLoc(width, height, (x+(j*2)+1), (y+(i*2))))
                    frame[(width * (y + (i * 2)))     + (x + (j * 2) + 1)] = value;
                if(validLoc(width, height, (x+(j*2)), (y+(i*2)+1)))
                    frame[(width * (y + (i * 2) + 1)) + (x + (j * 2))]     = value;
                if(validLoc(width, height, (x+(j*2)+1), (y+(i*2)+1)))
                    frame[(width * (y + (i * 2) + 1)) + (x + (j * 2) + 1)] = value;
            }
        }
    }
}

static void cCharacter_write(uint8_t *frame, int width, int height, char *string, int x, int y, int value)
{
    int i = 0;
    for(char c = string[i]; c != 0; c = string[++i]){
        cCharacter_draw_char(frame, width, height, c, x + (i * (CHAR_WIDTH + 1)), y, value);
    }
}

static void cCharacter_write_black(uint8_t *frame, int width, int height, char *string, int x, int y)
{
    cCharacter_write(frame, width, height, string, x, y, 0);
}

static void cCharacter_write_white(uint8_t *frame, int width, int height, char *string, int x, int y)
{
    cCharacter_write(frame, width, height, string, x, y, 255);
}

static void cCharacter_dwrite(uint8_t *frame, int width, int height, char *string, int x, int y, int value)
{
    int i = 0;
    for(char c = string[i]; c != 0; c = string[++i]){
        cCharacter_draw_dchar(frame, width, height, c, x + (i * ((CHAR_WIDTH * 2) + 2)), y, value);
    }
}
static void cCharacter_dwrite_black(uint8_t *frame, int width, int height, char *string, int x, int y)
{

    cCharacter_dwrite(frame, width, height, string, x, y, 0);
}

static void cCharacter_dwrite_white(uint8_t *frame, int width, int height, char *string, int x, int y)
{

    cCharacter_dwrite(frame, width, height, string, x, y, 255);
}

static void cCharacter_draw_plus(uint8_t *frame, int width, int height, int x, int y)
{

    int inner[9][2] = plus_inner_vals;

    for(int i = 0; i < 9; i++){
        if(validLoc(width, height, (x+inner[i][0]), (y+inner[i][1])))
            frame[((y+inner[i][1]) * width) + (x+inner[i][0])] = 255;
    }
    
    int outer[24][2] = plus_outer_vals;

    for(int i = 0; i < 23; i++){
        if(validLoc(width, height, (x+outer[i][0]), (y+outer[i][1])))
            frame[((y+outer[i][1]) * width) + (x+outer[i][0])] = 0;
    }
}
static void cCharacter_draw_plus_inverted(uint8_t *frame, int height, int width, int x, int y)
{

    int inner[9][2] = plus_inner_vals;

    for(int i = 0; i < 9; i++){
        if(validLoc(width, height, (x+inner[i][0]), (y+inner[i][1])))
            frame[((y+inner[i][1]) * width) + (x+inner[i][0])] = 0;
    }

    int outer[24][2] = plus_outer_vals;

    for(int i = 0; i < 23; i++){
        if(validLoc(width, height, (x+outer[i][0]), (y+outer[i][1])))
            frame[((y+outer[i][1]) * width) + (x+outer[i][0])] = 255;
    }
}
