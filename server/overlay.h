
#ifndef VIO_OVERLAY_H
#define VIO_OVERLAY_H

#include "cCharacter.h"

#define DRAW_BONUS_ROWS_TOP 32
#define DRAW_BONUS_ROWS_BOT 64
#define MAX_STRING_LEN		256

static int _draw_and_publish_vio_overlay(int overlay_ch, camera_image_metadata_t* m, \
		char* img, ext_vio_data_t d)
{
	static char* draw_frame = NULL;
	static int draw_frame_size = 0;

	camera_image_metadata_t draw_meta;
	int orig_bytes = 0;

	// dummy metadata if none was provided
	if(m==NULL){
		memset(&draw_meta, 0, sizeof(camera_image_metadata_t));
		draw_meta.height = 480 + DRAW_BONUS_ROWS_TOP + DRAW_BONUS_ROWS_BOT;
		draw_meta.width = 640;
		orig_bytes = 640 * 480;
		draw_meta.framerate = 0;
		draw_meta.timestamp_ns = _apps_time_monotonic_ns();
	}
	else{

		draw_meta = *m;
		orig_bytes = m->height * m->width;
		draw_meta.height = m->height + DRAW_BONUS_ROWS_TOP + DRAW_BONUS_ROWS_BOT;
	}

	draw_meta.format = IMAGE_FORMAT_RAW8;
	draw_meta.size_bytes = draw_meta.width * draw_meta.height;

	// allocate memory for the overlay if this is the first time through
	if(draw_frame == NULL){
		draw_frame = (char *)malloc(draw_meta.size_bytes);
		draw_frame_size = draw_meta.size_bytes;
	}
	else if(draw_frame_size<draw_meta.size_bytes){
		draw_frame = (char *)realloc(draw_frame, draw_meta.size_bytes);
		draw_frame_size = draw_meta.size_bytes;
	}


	// blank out the top rows
	memset(draw_frame, 0, draw_meta.width * DRAW_BONUS_ROWS_TOP);

	//memcpy image to global
	char* start_of_img = draw_frame + (draw_meta.width * DRAW_BONUS_ROWS_TOP);
	if(img !=NULL) memcpy(start_of_img, img, orig_bytes);
	else           memset(start_of_img, 0, orig_bytes);


	// blank out bottom rows
	char* start_of_bottom = draw_frame + (draw_meta.width * DRAW_BONUS_ROWS_TOP) + orig_bytes;
	memset(start_of_bottom, 0, draw_meta.width * DRAW_BONUS_ROWS_BOT);

	// draw top text
	char output_string[MAX_STRING_LEN];
	snprintf(output_string, MAX_STRING_LEN, "Qual:%3d%%  XYZ:%6.2lf %6.2lf %6.2lf  PTS:%3d",
								d.v.quality,\
								(double)d.v.T_imu_wrt_vio[0], \
								(double)d.v.T_imu_wrt_vio[1], \
								(double)d.v.T_imu_wrt_vio[2], \
								d.v.n_feature_points);

	cCharacter_dwrite_white((uint8_t*)draw_frame, draw_meta.width, draw_meta.height, output_string, 5, 9);



	// draw in-state and out-of-state feature points
	int oos_points = 0;
	for(uint32_t i=0;i<d.n_total_features;i++){
		int x = lroundf(d.features[i].pix_loc[0]);
		int y = lroundf(d.features[i].pix_loc[1]);
		vio_point_quality_t q = d.features[i].point_quality;

		if(x<=0 || y<=0) continue;

		// in-state points
		if(q == HIGH){
			cCharacter_draw_dchar((uint8_t*)draw_frame, draw_meta.width, draw_meta.height, \
				'O', x-4, y+DRAW_BONUS_ROWS_TOP - 6, 255);
		}
		else if(q == MEDIUM){
			oos_points++;
			cCharacter_draw_plus((uint8_t*)draw_frame, draw_meta.width, draw_meta.height, \
				x, y + DRAW_BONUS_ROWS_TOP);
		}
	}

	// draw bottom text
	snprintf(output_string, MAX_STRING_LEN, "EXP:%4.1fms  GAIN:%4d  OOS PTS:%3d  ERR: 0X%X",
						draw_meta.exposure_ns/1000000.0, draw_meta.gain, \
						oos_points, d.v.error_code);
	cCharacter_dwrite_white((uint8_t*)draw_frame, draw_meta.width, draw_meta.height, \
		output_string, 5, (draw_meta.height - DRAW_BONUS_ROWS_BOT) + 9);

	// error text
	char error_string[MAX_STRING_LEN];
	pipe_construct_vio_error_string(d.v.error_code, error_string, MAX_STRING_LEN);
	snprintf(output_string, MAX_STRING_LEN, "ERR: %s", error_string);
	cCharacter_dwrite_white((uint8_t*)draw_frame, draw_meta.width, draw_meta.height, \
		output_string, 5, (draw_meta.height - (DRAW_BONUS_ROWS_BOT/2)) + 9);


	pipe_server_write_camera_frame(overlay_ch, draw_meta, (char*) draw_frame);

	return 0;
}

#endif
