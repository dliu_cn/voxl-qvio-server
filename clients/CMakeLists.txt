cmake_minimum_required(VERSION 3.3)



set(TARGET voxl-inspect-qvio)
add_executable(${TARGET} voxl-inspect-qvio.c)
include_directories(../common)
list(APPEND ALL_TARGETS ${TARGET})


if(PLATFORM MATCHES QRB5165)
	set(CMAKE_LIBRARY_PATH /usr/lib32/)
	add_definitions(-DQRB5165 -D_GLIBCXX_USE_CXX11_ABI=0)
	file(GLOB LIB32 "/usr/lib32/lib*")
	target_link_libraries(${TARGET} modal_pipe m ${LIB32})
else()
	add_definitions(-DAPQ8096)
	target_link_libraries(${TARGET} modal_pipe m)
endif()






set(TARGET voxl-reset-qvio)
add_executable(${TARGET} voxl-reset-qvio.c)
include_directories(../common)
list(APPEND ALL_TARGETS ${TARGET})

if(PLATFORM MATCHES QRB5165)
	set(CMAKE_LIBRARY_PATH /usr/lib32/)
	add_definitions(-DQRB5165 -D_GLIBCXX_USE_CXX11_ABI=0)
	file(GLOB LIB32 "/usr/lib32/lib*")
	target_link_libraries(${TARGET} ${LIB32})
else()
	add_definitions(-DAPQ8096)
endif()



install(
	TARGETS ${ALL_TARGETS}
	LIBRARY			DESTINATION /usr/lib
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)
